package org.javaforever.infinity.domain;

import java.io.Serializable;

import org.javaforever.infinity.utils.StringUtil;

public class Type implements Serializable{
	private static final long serialVersionUID = 6754514447393057073L;
	protected String typeName;
	protected Domain domain;
	protected boolean templated = false;
	protected String packageToken;
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public Domain getDomain() {
		return domain;
	}
	public void setDomain(Domain domain) {
		this.domain = domain;
	}
	
	public String generateTypeString(){
		if (this.isTemplated())
			return (this.typeName + "<" + this.domain.getStandardName() + ">");
		else 
			return this.typeName;
	}
	public boolean isTemplated() {
		return templated;
	}
	public void setTemplated(boolean templated) {
		this.templated = templated;
	}
	public Type(){
		super();
	}
	public Type(String typeName, Domain domain,String packageToken){
		super();
		this.typeName = typeName;
		if (domain != null){
			this.domain = domain;
			this.templated = true;
			this.packageToken = packageToken;
		}
	}
	
	public Type(String typeName,String packageToken){
		super();
		this.typeName = typeName;
		this.packageToken = packageToken;
	}
	public String getPackageToken() {
		return packageToken;
	}
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	public Type(String typeName){
		super();
		this.typeName = typeName;
	}
	public boolean equals(Object o){
		Type rightType = (Type)o;
		if (this.typeName.equals(rightType.typeName) && this.domain.equals(rightType.getDomain()) && this.packageToken.equals(rightType.getPackageToken())){
			return true;
		}
		return false;
	}
	
	public String toString(){
		if (this.domain == null){
			return this.typeName;
		} else {
			if (this.typeName == null || "".equals(this.typeName)){
				return this.domain.getStandardName();
			}else {
				return this.typeName + "<" +this.domain.getStandardName() + ">";
			}
		}
	}
	
	public String toFullString(){
		if (this.domain == null){
			return this.typeName;
		} else {
			String ptoken = this.domain.getPackageToken() == null ?"":this.domain.getPackageToken();
			return (ptoken + this.domain.getStandardName());
		}
	}
	
	public boolean isLong(){
		return this.typeName.equalsIgnoreCase("long");
	}
	
	public boolean isInt(){
		return this.typeName.equals("int") || this.typeName.equals("Integer");
	}
	
	public static String getPrimeTypeGetterName(String typeName) {
		if (typeName.equalsIgnoreCase("String")) return ("getString");
		else if (typeName.equalsIgnoreCase("long")) return ("getLong");
		else if (typeName.equalsIgnoreCase("float")) return ("getFloat");
		else if (typeName.equalsIgnoreCase("double")) return ("getDouble");
		else if (typeName.equalsIgnoreCase("boolean")) return ("getBoolean");
		else if (typeName.equalsIgnoreCase("int") || typeName.equalsIgnoreCase("Integer")) return ("getInt");
		else return "get"+StringUtil.lowerFirst(typeName);
	}
	
	public static String getPrimeTypeSetterName(String typeName) {
		if (typeName.equalsIgnoreCase("String")) return ("setString");
		else if (typeName.equalsIgnoreCase("long")) return ("setLong");
		else if (typeName.equalsIgnoreCase("float")) return ("setFloat");
		else if (typeName.equalsIgnoreCase("double")) return ("setDouble");
		else if (typeName.equalsIgnoreCase("boolean")) return ("setBoolean");
		else if (typeName.equalsIgnoreCase("int") || typeName.equalsIgnoreCase("Integer")) return ("setInt");
		else return "set"+StringUtil.lowerFirst(typeName);
	}
	
	public static Type getClassType(String typeStr){
		if ("int".equals(typeStr)) return new Type("Integer");
		else if ("long".equals(typeStr)) return new Type("Long");
		else if ("boolean".equals(typeStr)) return new Type("Boolean");
		else if ("float".equals(typeStr)) return new Type("Float");
		else if ("double".equals(typeStr)) return new Type("Double");
		else return new Type(typeStr);
	}
	
	public static Type getOracleClassType(String typeStr){
		if ("int".equals(typeStr)) return new Type("Integer");
		else if ("long".equals(typeStr)) return new Type("String");
		else if ("boolean".equals(typeStr)) return new Type("Integer");
		else if ("float".equals(typeStr)) return new Type("Float");
		else if ("double".equals(typeStr)) return new Type("Double");
		else return new Type(typeStr);
	}
	
	public Type getClassType(){
		return Type.getClassType(this.typeName);
	}
	
	
	public String getTypeSetterName(){
			if ("int".equals(this.getClassType().getTypeName())|| "Integer".equals(this.getClassType().getTypeName())) return "setInt";
			else if ("long".equals(this.getClassType().getTypeName())|| "Long".equals(this.getClassType().getTypeName())) return "setLong";
			else if ("boolean".equals(this.getClassType().getTypeName()) ||"Boolean".equals(this.getClassType().getTypeName())) return "setBoolean";
			else if ("float".equals(this.getClassType().getTypeName())|| "Float".equals(this.getClassType().getTypeName())) return "setFloat";
			else if ("double".equals(this.getClassType().getTypeName())|| "Double".equals(this.getClassType().getTypeName())) return "setDouble";
			else return "";
	}
	
	
	public static String getInitTypeWithVal(Type type) {
		if (type.getClassType().getTypeName().equals("String")) return "\"\"";
		else if (type.getClassType().getTypeName().equalsIgnoreCase("long")) return"0L";
		else if (type.getClassType().getTypeName().equalsIgnoreCase("float")) return"0F";
		else if (type.getClassType().getTypeName().equalsIgnoreCase("double")) return"0D";
		else if (type.getClassType().getTypeName().equalsIgnoreCase("double")) return"false";
		else return "null";
	}
}
