package org.javaforever.infinity.utils;

import java.util.List;

import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.exception.ValidateException;

public class DomainUtil {
	public static Domain findDomainInList(List<Domain> targets, String domainName) throws ValidateException{
		for (Domain d: targets){
			if (d.getStandardName().equals(domainName)) return d;
		}
		throw new ValidateException("域对象"+domainName+"不在列表中！");
	}
}
