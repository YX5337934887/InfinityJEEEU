package org.javaforever.infinity.utils;

import org.javaforever.infinity.domain.Field;
import org.javaforever.infinity.domain.Var;

public class FieldUtil {
	public static String generateRequestGetParameterString(Field field, Var request){
		String type = field.getFieldType();
		System.out.println("JerryDebug:Here:"+type+":classType:"+field.getClassType()+":field:"+field.getFieldName());
		switch (type) {
			case "long": return "Long.parseLong(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
			case "int": return "Integer.parseInt(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
			case "float": return "Float.parseFloat(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
			case "double": return "Double.parseDouble(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
			case "String": return request.getVarName() + ".getParameter(\""+field.getFieldName()+"\")";
			case "boolean": return "Boolean.parseBoolean("+request.getVarName()+ ".getParameter(\""+field.getFieldName()+"\"))";
			
			case "Long": return "Long.parseLong(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
			case "Integer": return "Integer.parseInt(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
			case "Float": return "Float.parseFloat(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
			case "Double": return "Double.parseDouble(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
			case "Boolean": return "Boolean.parseBoolean("+request.getVarName()+ ".getParameter(\""+field.getFieldName()+"\"))";
			
			case "BigDecimal": return "new BigDecimal("+request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
			case "Timestamp": return "new Timestamp("+request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
			case "Date": return "new Date("+request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
			default : return null;
		}
	}
}