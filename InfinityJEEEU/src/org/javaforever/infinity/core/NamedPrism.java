package org.javaforever.infinity.core;

import org.javaforever.infinity.domain.Prism;
import org.javaforever.infinity.domain.ValidateInfo;

public class NamedPrism extends Prism implements Comparable<Prism>{
	
	@Override
	public ValidateInfo validate(){
		ValidateInfo info = new ValidateInfo();
		info.setSuccess(true);
		return info;
	}
	
}
