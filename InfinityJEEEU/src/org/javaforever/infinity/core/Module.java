package org.javaforever.infinity.core;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.infinity.domain.Noun;
import org.javaforever.infinity.domain.Prism;
import org.javaforever.infinity.domain.Util;
import org.javaforever.infinity.generator.DBDefinitionGenerator;

public class Module {
	protected String standardName;
	protected String packageToken;
	protected List<NamedPrism> prisms = new ArrayList<NamedPrism>();
	protected List<Util> utils = new ArrayList<Util>();
	protected List<DBDefinitionGenerator> dbDefinitionGenerators = new ArrayList<DBDefinitionGenerator>();
	protected List<Configable> configables = new ArrayList<Configable>();
	protected List<Noun> nouns = new ArrayList<Noun>();
	protected String dbPrefix = "";
	protected String label;
	protected List<List<Noun>> dataNouns = new ArrayList<List<Noun>>();
	protected String projectName;
	
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getPackageToken() {
		return packageToken;
	}
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	public List<Util> getUtils() {
		return utils;
	}
	public void setUtils(List<Util> utils) {
		this.utils = utils;
	}
	public List<DBDefinitionGenerator> getDbDefinitionGenerators() {
		return dbDefinitionGenerators;
	}
	public void setDbDefinitionGenerators(List<DBDefinitionGenerator> dbDefinitionGenerators) {
		this.dbDefinitionGenerators = dbDefinitionGenerators;
	}
	public List<Configable> getConfigables() {
		return configables;
	}
	public void setConfigables(List<Configable> configables) {
		this.configables = configables;
	}
	public List<Noun> getNouns() {
		return nouns;
	}
	public void setNouns(List<Noun> nouns) {
		this.nouns = nouns;
	}
	public String getDbPrefix() {
		return dbPrefix;
	}
	public void setDbPrefix(String dbPrefix) {
		this.dbPrefix = dbPrefix;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public List<List<Noun>> getDataNouns() {
		return dataNouns;
	}
	public void setDataNouns(List<List<Noun>> dataNouns) {
		this.dataNouns = dataNouns;
	}
	public List<NamedPrism> getPrisms() {
		return prisms;
	}
	public void setPrisms(List<NamedPrism> prisms) {
		this.prisms = prisms;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}
