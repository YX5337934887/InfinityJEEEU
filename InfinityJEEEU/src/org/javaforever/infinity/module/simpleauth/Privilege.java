package org.javaforever.infinity.module.simpleauth;

import org.javaforever.infinity.domain.Field;
import org.javaforever.infinity.domain.Noun;
import org.javaforever.infinity.domain.ValidateInfo;

public class Privilege extends Noun{
	
	public Privilege(){
		super();
		this.setStandardName("Privilege");
		this.setDomainId(new Field("privilegeId","Long"));
		this.setActive(new Field("active","Boolean"));
		this.setDomainName(new Field("privilegeName","String"));
		this.addField(new Field("canDelete","Boolean"));
		this.addField(new Field("url","String"));
		
		this.setDbPrefix("sa_");
		this.setPackageToken("org.javaforever.infinity.simpleauth");
	}
}
