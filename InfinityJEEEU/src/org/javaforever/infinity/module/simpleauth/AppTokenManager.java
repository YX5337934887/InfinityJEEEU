package org.javaforever.infinity.module.simpleauth;

import org.javaforever.infinity.domain.Util;

public class AppTokenManager extends Util{
	public AppTokenManager(){
		super();
		super.fileName = "AppTokenManager.java";
	}
	
	public AppTokenManager(String packageToken){
		super();
		this.setPackageToken(packageToken+".utils");
		super.fileName = "AppTokenManager.java";
	}
	
	@Override
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	@Override
	public String generateUtilString() {
		StringBuilder sb = new StringBuilder();
		sb.append("package "+this.getPackageToken()+";\n");
		sb.append("\n");
		sb.append("import java.util.concurrent.ConcurrentHashMap;\n");
		sb.append("\n");
		sb.append("public class AppTokenManager {\n");
		sb.append("final static long TOKEN_EXPIRES_TIME = 1800000L; // 30 minutes\n");
		sb.append("public static ConcurrentHashMap<String,UserRecord> onlineUsers = new ConcurrentHashMap<String,UserRecord>();\n");
		sb.append("\n");
		sb.append("public static  String addUserId(long userId){\n");
		sb.append("String token = generateANewToken();\n");
		sb.append("removeUserId(userId);\n");
		sb.append("UserRecord ur = new UserRecord();\n");
		sb.append("ur.setUserId(userId);\n");
		sb.append("ur.setMicroSecond(System.currentTimeMillis());\n");
		sb.append("onlineUsers.put(token, ur);\n");
		sb.append("return token;\n");
		sb.append("}\n");
		sb.append("\n");
		sb.append("public static  void removeUserId(long userId){\n");
		sb.append("for (String token : onlineUsers.keySet()){\n");
		sb.append("if (onlineUsers.get(token).getUserId() == userId) onlineUsers.remove(token);\n");
		sb.append("}\n");
		sb.append("}\n");
		sb.append("\n");
		sb.append("public static void removeUserIdByToken(String token){\n");
		sb.append("onlineUsers.remove(token);\n");
		sb.append("}\n");
		sb.append("\n");
		sb.append("public static boolean isValidUserId(long userId){\n");
		sb.append("for ( UserRecord ur : onlineUsers.values()){\n");
		sb.append("if (ur.getUserId()== userId && (System.currentTimeMillis()-ur.getMicroSecond())<= TOKEN_EXPIRES_TIME){// 30 minutes\n");
		sb.append("ur.setMicroSecond(System.currentTimeMillis());\n");
		sb.append("return true;\n");
		sb.append("}else if (ur.getUserId()== userId && (System.currentTimeMillis()-ur.getMicroSecond())> TOKEN_EXPIRES_TIME){\n");
		sb.append("removeUserId(userId);\n");		
		sb.append("}\n");
		sb.append("}\n");
		sb.append("return false;\n");
		sb.append("}\n");
		sb.append("\n");
		sb.append("public static long returnUserId(String token){\n");
		sb.append("try {\n");
		sb.append("long userId = onlineUsers.get(token).getUserId();\n");
		sb.append("if (isValidUserId(userId)) return userId;\n");
		sb.append("else return 0;\n");
		sb.append("} catch (Exception e){\n");
		sb.append("return 0;\n");
		sb.append("}\n");
		sb.append("}\n");
		sb.append("\n");
		sb.append("private static String generateANewToken() {\n");
		sb.append("int length = 64;\n");
		sb.append("StringBuilder tokenBuilder = new StringBuilder(\"\");\n");
		sb.append("String token;\n");
		sb.append("char[] tokenletters = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };\n");
		sb.append("\n");
		sb.append("do {\n");
		sb.append("for (int i = 0; i < length; i++) {\n");
		sb.append("tokenBuilder.append(tokenletters[(int) (Math.random() * 16)]);\n");
		sb.append("}\n");
		sb.append("token = tokenBuilder.toString();\n");
		sb.append("} while (AppTokenManager.onlineUsers.get(token) != null);\n");
		sb.append("return token;\n");
		sb.append("}\n");
		sb.append("\n");
		sb.append("private static void printOutOnlineUsers(){\n");
		sb.append("for (String token: onlineUsers.keySet()){\n");
		sb.append("UserRecord ur = onlineUsers.get(token);\n");
		sb.append("System.out.println(token + \":\"+ ur.getUserId() + \":\" + ur.getMicroSecond());\n");
		sb.append("}\n");
		sb.append("System.out.println();\n");
		sb.append("}\n");
		sb.append("\n");
		sb.append("public static void main(String [] args){\n");	
		sb.append("String token = addUserId(2L);\n");
		sb.append("printOutOnlineUsers();\n");
		sb.append("token=addUserId(2L);\n");
		sb.append("printOutOnlineUsers();\n");
		sb.append("String token2 = addUserId(3L);\n");
		sb.append("printOutOnlineUsers();\n");
		sb.append("System.out.println(\"IsValide:4:\"+ isValidUserId(4));\n");
		sb.append("System.out.println(\"IsValide:3:\"+ isValidUserId(3));\n");
		sb.append("removeUserId(3L);\n");
		sb.append("printOutOnlineUsers();\n");
		sb.append("System.out.println(\"IsValide:3:\"+ isValidUserId(3));\n");
		sb.append("long userId = returnUserId(token);\n");
		sb.append("System.out.println(\"UserId:\"+userId+\":\"+token);\n");
		sb.append("printOutOnlineUsers();\n");
		sb.append("\n");
		sb.append("userId = returnUserId(token2);\n");
		sb.append("}\n");
		sb.append("}\n");
		sb.append("\n");
		sb.append("class UserRecord {\n");
		sb.append("protected long userId;\n");
		sb.append("protected long microSecond;\n");
		sb.append("public long getUserId() {\n");
		sb.append("return userId;\n");
		sb.append("}\n");
		sb.append("public void setUserId(long userId) {\n");
		sb.append("this.userId = userId;\n");
		sb.append("}\n");
		sb.append("public long getMicroSecond() {\n");
		sb.append("return microSecond;\n");
		sb.append("}\n");
		sb.append("public void setMicroSecond(long microSecond) {\n");
		sb.append("this.microSecond = microSecond;\n");
		sb.append("}\n");
		sb.append("}\n");

		return sb.toString();
	}
}
