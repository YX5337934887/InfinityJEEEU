package org.javaforever.infinity.module.simpleauth;

import org.javaforever.infinity.domain.Field;
import org.javaforever.infinity.domain.Noun;

public class User  extends Noun{
	public User(){
		super();
		this.setStandardName("User");
		this.setDomainId(new Field("userId","Long"));
		this.setActive(new Field("active","Boolean"));
		this.setDomainName(new Field("userName","String"));
		this.addField(new Field("description","String"));
		this.addField(new Field("firstName","String"));
		this.addField(new Field("lastName","String"));
		this.addField(new Field("password","String"));
		this.addField(new Field("gender","String"));
		this.addField(new Field("salt","String"));
		this.addField(new Field("loginFailure","Integer"));
		this.addField(new Field("address","String"));
		this.addField(new Field("phone","String"));
		this.addField(new Field("mobile","String"));
		this.addField(new Field("confirmPassword","String"));
		
		this.setDbPrefix("sa_");
		this.setPackageToken("org.javaforever.infinity.simpleauth");
	}
}

