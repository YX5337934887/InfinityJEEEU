package org.javaforever.infinity.module.simpleauth;

import org.javaforever.infinity.domain.Field;
import org.javaforever.infinity.domain.Noun;

public class Role  extends Noun{
	
	public Role(){
		super();
		this.setStandardName("Role");
		this.setDomainId(new Field("roleId","Long"));
		this.setActive(new Field("active","Boolean"));
		this.setDomainName(new Field("roleName","String"));
		this.addField(new Field("description","String"));
		
		this.setDbPrefix("sa_");
		this.setPackageToken("org.javaforever.infinity.simpleauth");
	}

}
