package org.javaforever.infinity.module.simpleauth;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.javaforever.infinity.core.Module;
import org.javaforever.infinity.domain.Noun;
import org.javaforever.infinity.domain.Prism;
import org.javaforever.infinity.domain.Util;
import org.javaforever.infinity.exception.ValidateException;
import org.javaforever.infinity.utils.StringUtil;

public class SimpleAuthModule extends Module{
	protected Privilege privilege;
	protected Role role;
	protected User user;
	protected PrivilegePrism privilegePrism;
	protected RolePrism rolePrism;
	protected UserPrism userPrism;
	
	public SimpleAuthModule(){
		super();
		Privilege privilege = new Privilege();
		Role role = new Role();
		User user = new User();
		this.nouns.add(privilege);
		this.nouns.add(role);
		this.nouns.add(user);
		this.packageToken = "org.javaforever.infinity.simpleauth";
		this.privilege = privilege;
		this.role = role;
		this.user = user;
		this.projectName = "SimpleAuth";
		this.standardName = "SimpleAuth";
	}
	
	public void generateNamedPrismsFromNouns() throws Exception{
		PrivilegePrism privilegePrism = new PrivilegePrism();
		privilegePrism.generateNamedPrismFromNoun(this.privilege);
		
		RolePrism rolePrism = new RolePrism();
		rolePrism.generateNamedPrismFromNoun(this.role);
		
		UserPrism userPrism = new UserPrism();
		userPrism.generateNamedPrismFromNoun(this.user);
		
		this.prisms.add(privilegePrism);
		this.prisms.add(rolePrism);
		this.prisms.add(userPrism);		
	}
	
	public void generateModuleFiles() throws ValidateException{
		try {
			String srcfolderPath = "C:/JerryWork/InfinityJEEEU/source/"+this.getProjectName()+"/";
			String mypath = "";
			for (Noun n: this.nouns){				
				if (this.packageToken != null && !"".equals(this.packageToken))
					mypath = srcfolderPath + "src/" + StringUtil.packagetokenToFolder(this.packageToken)+"/domain/";
				String filePath = mypath+n.getStandardName()+".java";
				writeToFile(filePath, n.generateClassString());
			}
			
			for (Prism ps: this.prisms){
				ps.setFolderPath(srcfolderPath);
				ps.generatePrismFiles();
			}
			for (Util u:utils){
				String utilPath = srcfolderPath + "src/" + StringUtil.packagetokenToFolder(u.getPackageToken());
				String utilFilePath = utilPath+u.getFileName();
				writeToFile(utilFilePath, u.generateUtilString());
			}
		} catch (Exception e){
			e.printStackTrace();
			throw new ValidateException("源码生成错误");
		}
	}
	
	public void writeToFile(String filePath, String content) throws Exception{
		File f = new File(filePath);
		if (!f.getParentFile().exists()) {
			f.getParentFile().mkdirs();
		}
		f.createNewFile();
		try (Writer fw = new BufferedWriter( new OutputStreamWriter(new FileOutputStream(f.getAbsolutePath()),"UTF-8"))){			
	        fw.write(content,0,content.length());
		}
	}
	
	public static void main(String [] argv) throws Exception{
		SimpleAuthModule sam = new SimpleAuthModule();
		sam.generateNamedPrismsFromNouns();
		sam.generateModuleFiles();		
	}
}
