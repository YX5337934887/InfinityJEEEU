package org.javaforever.infinity.module.simpleauth;

import org.javaforever.infinity.core.NamedPrism;
import org.javaforever.infinity.core.Verb;
import org.javaforever.infinity.domain.Dao;
import org.javaforever.infinity.domain.DaoImpl;
import org.javaforever.infinity.domain.Facade;
import org.javaforever.infinity.domain.ManyToMany;
import org.javaforever.infinity.domain.Method;
import org.javaforever.infinity.domain.Service;
import org.javaforever.infinity.domain.ServiceImpl;
import org.javaforever.infinity.domain.Type;
import org.javaforever.infinity.domain.ValidateInfo;
import org.javaforever.infinity.easyui.EasyUIPageTemplate;
import org.javaforever.infinity.exception.ValidateException;
import org.javaforever.infinity.generator.DBDefinitionFactory;
import org.javaforever.infinity.generator.DBDefinitionGenerator;
import org.javaforever.infinity.generator.NamedUtilMethodGenerator;
import org.javaforever.infinity.limitedverb.CountActiveRecords;
import org.javaforever.infinity.limitedverb.CountAllRecords;
import org.javaforever.infinity.limitedverb.CountPage;
import org.javaforever.infinity.limitedverb.CountSearchByFieldsRecords;
import org.javaforever.infinity.limitedverb.DaoOnlyVerb;
import org.javaforever.infinity.limitedverb.NoControllerVerb;
import org.javaforever.infinity.verb.Add;
import org.javaforever.infinity.verb.Delete;
import org.javaforever.infinity.verb.DeleteAll;
import org.javaforever.infinity.verb.FindById;
import org.javaforever.infinity.verb.FindByName;
import org.javaforever.infinity.verb.ListActive;
import org.javaforever.infinity.verb.ListAll;
import org.javaforever.infinity.verb.ListAllByPage;
import org.javaforever.infinity.verb.SearchByFieldsByPage;
import org.javaforever.infinity.verb.SearchByName;
import org.javaforever.infinity.verb.SoftDelete;
import org.javaforever.infinity.verb.SoftDeleteAll;
import org.javaforever.infinity.verb.Toggle;
import org.javaforever.infinity.verb.ToggleOne;
import org.javaforever.infinity.verb.Update;

public class UserPrism extends NamedPrism{
	
	public UserPrism(){
		super();
		this.packageToken = "org.javaforever.infinity.simpleauth";
	}

	
	public void generateNamedPrismFromNoun(User user) throws Exception{
		this.setDomain(user);
		Dao userdao = new Dao();
		userdao.setDomain(user);
		userdao.setPackageToken(user.getPackageToken());
		DaoImpl userdaoimpl = new DaoImpl();
		userdaoimpl.setPackageToken(user.getPackageToken());
		userdaoimpl.setDomain(user);
		userdaoimpl.setDao(userdao);
		this.setDao(userdao);
		this.setDaoimpl(userdaoimpl);
		
		Service userservice = new Service();
		userservice.setDomain(user);
		userservice.setPackageToken(user.getPackageToken());
		ServiceImpl userserviceimpl = new ServiceImpl(user);
		userserviceimpl.setDomain(user);
		userserviceimpl.setPackageToken(user.getPackageToken());
		userserviceimpl.setService(userservice);
		this.setService(userservice);
		this.setServiceimpl(userserviceimpl);
		
		Verb listAll = new ListAll(user);
		Verb update = new Update(user);
		Verb delete = new Delete(user);
		Verb add = new Add(user);
		Verb softdelete = new SoftDelete(user);
		Verb findbyid = new FindById(user);
		Verb findbyname = new FindByName(user);
		Verb searchbyname = new SearchByName(user);
		Verb listactive = new ListActive(user);
		Verb listAllByPage = new ListAllByPage(user);
		Verb deleteAll = new DeleteAll(user);
		Verb softDeleteAll = new SoftDeleteAll(user);
		Verb toggle = new Toggle(user);
		Verb toggleOne = new ToggleOne(user);
		Verb searchByFieldsByPage = new SearchByFieldsByPage(user);
		
		CountPage countPage = new CountPage(user);
		CountActiveRecords countActiveRecords = new CountActiveRecords(user);
		CountAllRecords countAllRecords = new CountAllRecords(user);
		CountSearchByFieldsRecords countSearch = new CountSearchByFieldsRecords(user);
		
		this.addVerb(listAll);
		this.addVerb(update);
		this.addVerb(delete);
		this.addVerb(add);
		this.addVerb(softdelete);
		this.addVerb(findbyid);
		this.addVerb(findbyname);
		this.addVerb(searchbyname);
		this.addVerb(listactive);
		this.addVerb(listAllByPage);
		this.addVerb(deleteAll);
		this.addVerb(softDeleteAll);
		this.addVerb(toggle);
		this.addVerb(toggleOne);
		this.addVerb(searchByFieldsByPage);
		
		this.noControllerVerbs.add(countPage);
		this.noControllerVerbs.add(countActiveRecords);
		this.noControllerVerbs.add(countAllRecords);
		this.noControllerVerbs.add(countSearch);
		
		for (Verb v: this.verbs){
			v.setDomain(user);
			Facade facade = new Facade(v,user);
			facade.setPackageToken(user.getPackageToken());
			this.facades.add(facade);
			service.addMethod(v.generateServiceMethodDefinition());
			serviceimpl.addMethod(v.generateServiceImplMethod());
			dao.addMethod(v.generateDaoMethodDefinition());
			daoimpl.addMethod(v.generateDaoImplMethod());
		}
		
		for (NoControllerVerb nVerb: this.noControllerVerbs){
			nVerb.setDomain(user);
			service.addMethod(nVerb.generateServiceMethodDefinition());
			serviceimpl.addMethod(nVerb.generateServiceImplMethod());
			dao.addMethod(nVerb.generateDaoMethodDefinition());
			daoimpl.addMethod(nVerb.generateDaoImplMethod());				
		}
		
		for (DaoOnlyVerb oVerb: this.daoOnlyVerbs){
			oVerb.setDomain(user);
			dao.addMethod(oVerb.generateDaoMethodDefinition());
			daoimpl.addMethod(oVerb.generateDaoImplMethod());				
		}
		
		DBDefinitionGenerator dbg = DBDefinitionFactory.getInstance("mysql");
		dbg.addDomain(user);
		dbg.setDbName(user.getStandardName());
		this.setDbDefinitionGenerator(dbg);	
							
		EasyUIPageTemplate easyui = new EasyUIPageTemplate();
		easyui.setDomain(user);
		easyui.setStandardName(user.getStandardName().toLowerCase());
		this.addEuTemplate(easyui);
		
		if (user.getManyToManySlaveNames()!= null && user.getManyToManySlaveNames().size() > 0) {
			for (String slaveName : user.getManyToManySlaveNames()) {
				String masterName = user.getStandardName();
				if (setContainsDomain(this.projectDomains, masterName)
						&& setContainsDomain(this.projectDomains, slaveName)) {
					this.manyToManies.add(new ManyToMany(lookupDoaminInSet(this.projectDomains, masterName),
							lookupDoaminInSet(this.projectDomains, slaveName)));
				} else {
					ValidateInfo validateInfo = new ValidateInfo();
					validateInfo.addCompileError("棱柱" + this.getStandardName() + "多对多设置有误。");
					ValidateException em = new ValidateException(validateInfo);
					throw em;
				}
			}
		}
		for (ManyToMany mtm : this.manyToManies) {
			this.service.addMethod(mtm.getAssign().generateServiceMethodDefinition());
			this.serviceimpl.addMethod(mtm.getAssign().generateServiceImplMethod());
			this.dao.addMethod(mtm.getAssign().generateDaoMethodDefinition());
			this.daoimpl.addMethod(mtm.getAssign().generateDaoImplMethod());
			//this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getAssign().generateDaoImplMethod());
			//this.facade.addMethod(mtm.getAssign().generateFacadeMethod());

			this.service.addMethod(mtm.getRevoke().generateServiceMethodDefinition());
			this.serviceimpl.addMethod(mtm.getRevoke().generateServiceImplMethod());
			this.dao.addMethod(mtm.getRevoke().generateDaoMethodDefinition());
			this.daoimpl.addMethod(mtm.getRevoke().generateDaoImplMethod());
			//this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getRevoke().generateDaoImplMethod());
			//this.facade.addMethod(mtm.getRevoke().generateFacadeMethod());

			this.service.addMethod(mtm.getListMyActive().generateServiceMethodDefinition());
			this.serviceimpl.addMethod(mtm.getListMyActive().generateServiceImplMethod());
			this.dao.addMethod(mtm.getListMyActive().generateDaoMethodDefinition());
			this.daoimpl.addMethod(mtm.getListMyActive().generateDaoImplMethod());
//			this.mybatisDaoXmlDecorator.addResultMap(mtm.getSlave());
//			this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getListMyActive().generateDaoImplMethod());
//			this.facade.addMethod(mtm.getListMyActive().generateFacadeMethod());

			this.service.addMethod(mtm.getListMyAvailableActive().generateServiceMethodDefinition());
			this.serviceimpl.addMethod(mtm.getListMyAvailableActive().generateServiceImplMethod());
			this.dao.addMethod(mtm.getListMyAvailableActive().generateDaoMethodDefinition());
			this.daoimpl.addMethod(mtm.getListMyAvailableActive().generateDaoImplMethod());
//			this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getListMyAvailableActive().generateDaoImplMethod());
//			this.facade.addMethod(mtm.getListMyAvailableActive().generateFacadeMethod());
			mtm.getSlave().decorateCompareTo();
			
			Service slaveService = new Service();
			slaveService.setDomain(mtm.getSlave());
			slaveService.setStandardName(mtm.getSlave().getCapFirstDomainName() + "Service");
			
			Method slaveServiceSetter = NamedUtilMethodGenerator.generateSetter(mtm.getSlave().getLowerFirstDomainName()+"Service",
					new Type(mtm.getSlave().getCapFirstDomainName() + "Service",mtm.getSlave().getPackageToken()+".service."+mtm.getSlave().getCapFirstDomainName() + "Service"));
			this.serviceimpl.addMethod(slaveServiceSetter);
			this.serviceimpl.addClassImports(mtm.getSlave().getPackageToken()+".serviceimpl."+mtm.getSlave().getCapFirstDomainName() + "ServiceImpl");
			this.serviceimpl.addOtherService(slaveService);
	}
}
}
