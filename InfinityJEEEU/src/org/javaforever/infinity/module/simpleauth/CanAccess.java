package org.javaforever.infinity.module.simpleauth;

import org.javaforever.infinity.core.Verb;
import org.javaforever.infinity.domain.Method;

public class CanAccess extends Verb{

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		return null;
	}

	@Override
	public String generateControllerMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateFacadeMethod() throws Exception {
		return null;
	}

	@Override
	public String generateFacadeMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateFacadeMethodStringWithSerial() throws Exception {
		return null;
	}

}
