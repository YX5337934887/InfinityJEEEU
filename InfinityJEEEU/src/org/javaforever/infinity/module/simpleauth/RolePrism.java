package org.javaforever.infinity.module.simpleauth;

import org.javaforever.infinity.core.NamedPrism;
import org.javaforever.infinity.core.Verb;
import org.javaforever.infinity.domain.Dao;
import org.javaforever.infinity.domain.DaoImpl;
import org.javaforever.infinity.domain.Facade;
import org.javaforever.infinity.domain.ManyToMany;
import org.javaforever.infinity.domain.Method;
import org.javaforever.infinity.domain.Service;
import org.javaforever.infinity.domain.ServiceImpl;
import org.javaforever.infinity.domain.Type;
import org.javaforever.infinity.domain.ValidateInfo;
import org.javaforever.infinity.easyui.EasyUIPageTemplate;
import org.javaforever.infinity.exception.ValidateException;
import org.javaforever.infinity.generator.DBDefinitionFactory;
import org.javaforever.infinity.generator.DBDefinitionGenerator;
import org.javaforever.infinity.generator.NamedUtilMethodGenerator;
import org.javaforever.infinity.limitedverb.CountActiveRecords;
import org.javaforever.infinity.limitedverb.CountAllRecords;
import org.javaforever.infinity.limitedverb.CountPage;
import org.javaforever.infinity.limitedverb.CountSearchByFieldsRecords;
import org.javaforever.infinity.limitedverb.DaoOnlyVerb;
import org.javaforever.infinity.limitedverb.NoControllerVerb;
import org.javaforever.infinity.verb.Add;
import org.javaforever.infinity.verb.Delete;
import org.javaforever.infinity.verb.DeleteAll;
import org.javaforever.infinity.verb.FindById;
import org.javaforever.infinity.verb.FindByName;
import org.javaforever.infinity.verb.ListActive;
import org.javaforever.infinity.verb.ListAll;
import org.javaforever.infinity.verb.ListAllByPage;
import org.javaforever.infinity.verb.SearchByFieldsByPage;
import org.javaforever.infinity.verb.SearchByName;
import org.javaforever.infinity.verb.SoftDelete;
import org.javaforever.infinity.verb.SoftDeleteAll;
import org.javaforever.infinity.verb.Toggle;
import org.javaforever.infinity.verb.ToggleOne;
import org.javaforever.infinity.verb.Update;

public class RolePrism extends NamedPrism{
	
	public RolePrism(){
		super();
		this.packageToken = "org.javaforever.infinity.simpleauth";
	}

	public void generateNamedPrismFromNoun(Role role) throws Exception{
		this.setDomain(role);
		Dao roledao = new Dao();
		roledao.setDomain(role);
		roledao.setPackageToken(role.getPackageToken());
		DaoImpl roledaoimpl = new DaoImpl();
		roledaoimpl.setPackageToken(role.getPackageToken());
		roledaoimpl.setDomain(role);
		roledaoimpl.setDao(roledao);
		this.setDao(roledao);
		this.setDaoimpl(roledaoimpl);
		
		Service roleservice = new Service();
		roleservice.setDomain(role);
		roleservice.setPackageToken(role.getPackageToken());
		ServiceImpl roleserviceimpl = new ServiceImpl(role);
		roleserviceimpl.setDomain(role);
		roleserviceimpl.setPackageToken(role.getPackageToken());
		roleserviceimpl.setService(roleservice);
		this.setService(roleservice);
		this.setServiceimpl(roleserviceimpl);
		
		Verb listAll = new ListAll(role);
		Verb update = new Update(role);
		Verb delete = new Delete(role);
		Verb add = new Add(role);
		Verb softdelete = new SoftDelete(role);
		Verb findbyid = new FindById(role);
		Verb findbyname = new FindByName(role);
		Verb searchbyname = new SearchByName(role);
		Verb listactive = new ListActive(role);
		Verb listAllByPage = new ListAllByPage(role);
		Verb deleteAll = new DeleteAll(role);
		Verb softDeleteAll = new SoftDeleteAll(role);
		Verb toggle = new Toggle(role);
		Verb toggleOne = new ToggleOne(role);
		Verb searchByFieldsByPage = new SearchByFieldsByPage(role);
		
		CountPage countPage = new CountPage(role);
		CountActiveRecords countActiveRecords = new CountActiveRecords(role);
		CountAllRecords countAllRecords = new CountAllRecords(role);
		CountSearchByFieldsRecords countSearch = new CountSearchByFieldsRecords(role);
		
		this.addVerb(listAll);
		this.addVerb(update);
		this.addVerb(delete);
		this.addVerb(add);
		this.addVerb(softdelete);
		this.addVerb(findbyid);
		this.addVerb(findbyname);
		this.addVerb(searchbyname);
		this.addVerb(listactive);
		this.addVerb(listAllByPage);
		this.addVerb(deleteAll);
		this.addVerb(softDeleteAll);
		this.addVerb(toggle);
		this.addVerb(toggleOne);
		this.addVerb(searchByFieldsByPage);
		
		this.noControllerVerbs.add(countPage);
		this.noControllerVerbs.add(countActiveRecords);
		this.noControllerVerbs.add(countAllRecords);
		this.noControllerVerbs.add(countSearch);
		
		for (Verb v: this.verbs){
			v.setDomain(role);
			Facade facade = new Facade(v,role);
			facade.setPackageToken(role.getPackageToken());
			this.facades.add(facade);
			service.addMethod(v.generateServiceMethodDefinition());
			serviceimpl.addMethod(v.generateServiceImplMethod());
			dao.addMethod(v.generateDaoMethodDefinition());
			daoimpl.addMethod(v.generateDaoImplMethod());
		}
		
		for (NoControllerVerb nVerb: this.noControllerVerbs){
			nVerb.setDomain(role);
			service.addMethod(nVerb.generateServiceMethodDefinition());
			serviceimpl.addMethod(nVerb.generateServiceImplMethod());
			dao.addMethod(nVerb.generateDaoMethodDefinition());
			daoimpl.addMethod(nVerb.generateDaoImplMethod());				
		}
		
		for (DaoOnlyVerb oVerb: this.daoOnlyVerbs){
			oVerb.setDomain(role);
			dao.addMethod(oVerb.generateDaoMethodDefinition());
			daoimpl.addMethod(oVerb.generateDaoImplMethod());				
		}
		
		DBDefinitionGenerator dbg = DBDefinitionFactory.getInstance("mysql");
		dbg.addDomain(role);
		dbg.setDbName(role.getStandardName());
		this.setDbDefinitionGenerator(dbg);	
							
		EasyUIPageTemplate easyui = new EasyUIPageTemplate();
		easyui.setDomain(role);
		easyui.setStandardName(role.getStandardName().toLowerCase());
		this.addEuTemplate(easyui);
		
		if (role.getManyToManySlaveNames()!= null && role.getManyToManySlaveNames().size() > 0) {
			for (String slaveName : role.getManyToManySlaveNames()) {
				String masterName = role.getStandardName();
				if (setContainsDomain(this.projectDomains, masterName)
						&& setContainsDomain(this.projectDomains, slaveName)) {
					this.manyToManies.add(new ManyToMany(lookupDoaminInSet(this.projectDomains, masterName),
							lookupDoaminInSet(this.projectDomains, slaveName)));
				} else {
					ValidateInfo validateInfo = new ValidateInfo();
					validateInfo.addCompileError("棱柱" + this.getStandardName() + "多对多设置有误。");
					ValidateException em = new ValidateException(validateInfo);
					throw em;
				}
			}
		}
		for (ManyToMany mtm : this.manyToManies) {
			this.service.addMethod(mtm.getAssign().generateServiceMethodDefinition());
			this.serviceimpl.addMethod(mtm.getAssign().generateServiceImplMethod());
			this.dao.addMethod(mtm.getAssign().generateDaoMethodDefinition());
			this.daoimpl.addMethod(mtm.getAssign().generateDaoImplMethod());
			//this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getAssign().generateDaoImplMethod());
			//this.facade.addMethod(mtm.getAssign().generateFacadeMethod());

			this.service.addMethod(mtm.getRevoke().generateServiceMethodDefinition());
			this.serviceimpl.addMethod(mtm.getRevoke().generateServiceImplMethod());
			this.dao.addMethod(mtm.getRevoke().generateDaoMethodDefinition());
			this.daoimpl.addMethod(mtm.getRevoke().generateDaoImplMethod());
			//this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getRevoke().generateDaoImplMethod());
			//this.facade.addMethod(mtm.getRevoke().generateFacadeMethod());

			this.service.addMethod(mtm.getListMyActive().generateServiceMethodDefinition());
			this.serviceimpl.addMethod(mtm.getListMyActive().generateServiceImplMethod());
			this.dao.addMethod(mtm.getListMyActive().generateDaoMethodDefinition());
			this.daoimpl.addMethod(mtm.getListMyActive().generateDaoImplMethod());
//			this.mybatisDaoXmlDecorator.addResultMap(mtm.getSlave());
//			this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getListMyActive().generateDaoImplMethod());
//			this.facade.addMethod(mtm.getListMyActive().generateFacadeMethod());

			this.service.addMethod(mtm.getListMyAvailableActive().generateServiceMethodDefinition());
			this.serviceimpl.addMethod(mtm.getListMyAvailableActive().generateServiceImplMethod());
			this.dao.addMethod(mtm.getListMyAvailableActive().generateDaoMethodDefinition());
			this.daoimpl.addMethod(mtm.getListMyAvailableActive().generateDaoImplMethod());
//			this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getListMyAvailableActive().generateDaoImplMethod());
//			this.facade.addMethod(mtm.getListMyAvailableActive().generateFacadeMethod());
			mtm.getSlave().decorateCompareTo();
			
			Service slaveService = new Service();
			slaveService.setDomain(mtm.getSlave());
			slaveService.setStandardName(mtm.getSlave().getCapFirstDomainName() + "Service");
			
			Method slaveServiceSetter = NamedUtilMethodGenerator.generateSetter(mtm.getSlave().getLowerFirstDomainName()+"Service",
					new Type(mtm.getSlave().getCapFirstDomainName() + "Service",mtm.getSlave().getPackageToken()+".service."+mtm.getSlave().getCapFirstDomainName() + "Service"));
			this.serviceimpl.addMethod(slaveServiceSetter);
			this.serviceimpl.addClassImports(mtm.getSlave().getPackageToken()+".serviceimpl."+mtm.getSlave().getCapFirstDomainName() + "ServiceImpl");
			this.serviceimpl.addOtherService(slaveService);
		}
	}
}
