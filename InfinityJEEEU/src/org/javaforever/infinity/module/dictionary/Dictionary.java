package org.javaforever.infinity.module.dictionary;

import org.javaforever.infinity.domain.Field;
import org.javaforever.infinity.domain.Noun;

public class Dictionary extends Noun{
	
	public Dictionary(){
		super();
		this.setStandardName("Dictionary");
		this.setPlural("Dictionaries");
		this.setDomainId(new Field("dictionaryId","Long"));
		this.setActive(new Field("active","Boolean"));
		this.setDomainName(new Field("dictionaryName","String"));
		this.addField(new Field("parentId","long"));
		this.addField(new Field("dicGroupId","long"));
		this.addField(new Field("dictionaryKey","String"));
		this.addField(new Field("dictionaryValue","String"));
		this.addField(new Field("description","String"));
		
		this.setDbPrefix("dic_");
		this.setPackageToken("org.javaforever.infinity.dictionary");
	}
}
