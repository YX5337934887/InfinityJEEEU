package org.javaforever.infinity.module.dictionary;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.javaforever.infinity.core.Module;
import org.javaforever.infinity.domain.Noun;
import org.javaforever.infinity.domain.Prism;
import org.javaforever.infinity.domain.Util;
import org.javaforever.infinity.exception.ValidateException;
import org.javaforever.infinity.utils.StringUtil;

public class DictionaryModule extends Module{
	protected DicGroup dicGroup;
	protected Dictionary dictionary;
	protected DicGroupPrism dicGroupPrism;
	protected DictionaryPrism dictionaryPrism;

	
	public DictionaryModule(){
		super();
		DicGroup dicGroup = new DicGroup();
		Dictionary dictionary = new Dictionary();

		this.nouns.add(dicGroup);
		this.nouns.add(dictionary);
		this.packageToken = "org.javaforever.infinity.dictionary";
		this.dicGroup = dicGroup;
		this.dictionary = dictionary;
		this.projectName = "Dictionary";
		this.standardName = "Dictionary";
	}
	
	public void generateNamedPrismsFromNouns() throws Exception{
		DicGroupPrism dicGroupPrism = new DicGroupPrism();
		dicGroupPrism.generateNamedPrismFromNoun(this.dicGroup);
		
		DictionaryPrism dictionaryPrism = new DictionaryPrism();
		dictionaryPrism.generateNamedPrismFromNoun(this.dictionary);
		
		this.prisms.add(dicGroupPrism);
		this.prisms.add(dictionaryPrism);

	}
	
	public void generateModuleFiles() throws ValidateException{
		try {
			String srcfolderPath = "C:/JerryWork/InfinityJEEEU/source/"+this.getProjectName()+"/";
			String mypath = "";
			for (Noun n: this.nouns){				
				if (this.packageToken != null && !"".equals(this.packageToken))
					mypath = srcfolderPath + "src/" + StringUtil.packagetokenToFolder(this.packageToken)+"/domain/";
				String filePath = mypath+n.getStandardName()+".java";
				writeToFile(filePath, n.generateClassString());
			}
			
			for (Prism ps: this.prisms){
				ps.setFolderPath(srcfolderPath);
				ps.generatePrismFiles();
			}
			for (Util u:utils){
				String utilPath = srcfolderPath + "src/" + StringUtil.packagetokenToFolder(u.getPackageToken());
				String utilFilePath = utilPath+u.getFileName();
				writeToFile(utilFilePath, u.generateUtilString());
			}
		} catch (Exception e){
			e.printStackTrace();
			throw new ValidateException("源码生成错误");
		}
	}
	
	public void writeToFile(String filePath, String content) throws Exception{
		File f = new File(filePath);
		if (!f.getParentFile().exists()) {
			f.getParentFile().mkdirs();
		}
		f.createNewFile();
		try (Writer fw = new BufferedWriter( new OutputStreamWriter(new FileOutputStream(f.getAbsolutePath()),"UTF-8"))){			
	        fw.write(content,0,content.length());
		}
	}
	
	public static void main(String [] argv) throws Exception{
		DictionaryModule sam = new DictionaryModule();
		sam.generateNamedPrismsFromNouns();
		sam.generateModuleFiles();		
	}
}
