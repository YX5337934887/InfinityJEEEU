package org.javaforever.infinity.module.dictionary;

import org.javaforever.infinity.domain.Field;
import org.javaforever.infinity.domain.Noun;

public class DicGroup extends Noun{
	
	public DicGroup(){
		super();
		this.setStandardName("DicGroup");
		this.setDomainId(new Field("dicGroupId","Long"));
		this.setActive(new Field("active","Boolean"));
		this.setDomainName(new Field("dicGroupName","String"));
		this.addField(new Field("parentId","long"));
		this.addField(new Field("dicGroupKey","String"));
		this.addField(new Field("dicGroupValue","String"));
		this.addField(new Field("description","String"));
		
		this.setDbPrefix("dic_");
		this.setPackageToken("org.javaforever.infinity.dictionary");
	}

}
